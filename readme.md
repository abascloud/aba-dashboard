aba-dashboard
=============

## Installation

    bower install --save git@bitbucket.org:abascloud/aba-dashboard.git

## Demo

1. Clone repository

        git clone git@bitbucket.org:abascloud/aba-dashboard.git

1. Install Polymer-cli

        npm install -g polymer-cli

1. Start webserver

        cd ./aba-dashboard
        polymer serve

1. Install dependencies

        bower install

1. Browse to the demo page

        http://127.0.0.1:8081/components/aba-dashboard/demo/index.html?dashboard=demo/bigboard.json

## Widget Development
### Creating a widget
1. Create a repository for your new widget and clone it into bower_components

1. Change directory to your newly cloned repository

        cd ./bower_components/your-new-repo/

1. Create a bower file for it

        bower init

1. Add following dependencies to it (mostlikly no need to aktually install them now since the dashboard also will install these)

        "dependencies": {
            "aba-widget": "git@bitbucket.org:abascloud/aba-widget.git#^2.0.2",
            "aba-widget-behavior": "git@bitbucket.org:abascloud/aba-widget-behavior.git#^2.0.6",
            "abas-styles": "git@bitbucket.org:abascloud/abas-styles.git"
        }

1. Create a WebComponent file based on this Template

    Class based:

        <link rel="import" href="../aba-widget/aba-widget.html">
        <link rel="import" href="../aba-widget-behavior/aba-widget-behavior.html">
        <link rel="import" href="../abas-styles/abas-styles.html">
        <link rel="import" href="../polymer/polymer.html">

        <dom-module id="my-widget">
            <template>
                <custom-style></custom-style>

                <aba-widget>
                    <div slot="content">
                        <!-- Widget content goes here! -->
                    </div>
                    <div slot="settingsForm">
                        <!-- Widget settings goes here! -->
                    </div>
                </aba-widget>
            </template>

            <script>
                class MyWidget extends ABAS.abaWidgetBehavior(Polymer.Element) {
                    static get is() { return 'my-widget' }
                }
                customElements.define(MyWidget.is, MyWidget);
            </script>
        </dom-module>

    You can find a more dedicated example [here](https://bitbucket.org/abascloud/aba-erpaction-widget/src/b44d71a8a71bf25d2adc39aa9fd394969e1cf5df/aba-erpaction-widget.html?at=master&fileviewer=file-view-default)

    Inline Template:

        <link rel="import" href="../polymer/polymer.html">
        <link rel="import" href="../aba-widget-behavior/aba-widget-inline-behavior.html">
        <link rel="import" href="../aba-widget/aba-widget-inline.html">
        <link rel="import" href="../abas-styles/abas-styles.html">
        <script>
            class MyWidget extends ABAS.abaWidgetInlineBehavior(AbaWidgetInline) {
                constructor() {
                    super();
                }

                static get content() {
                    return `<!-- Widget content goes here! -->`;
                }

                static get settingsForm() {
                    return `<!-- Widget settings goes here! -->`;
                }

                static get style() {
                    return `<!-- Css goes here! -->`;
                }
            }
            customElements.define('my-widget', MyWidget);
        </script>

    You can find a more dedicated example [here](https://bitbucket.org/abascloud/aba-dummy-widget/src/8024a1b6d46151e1a6637ef9501b9f530a14bd62/aba-dummy-widget.html?at=master&fileviewer=file-view-default)

1. Add your new Widget to demo/widgets.json

        {
            "name": "widget-tag-name",
            "description": "Lorem Ipsum",
            "preview": "path/to/a/preview/image.jpg",
            "author": "abas Software GmbH",
            "label": "Label for the Widget Shop",
            "path": "../path-to-your/widget.html",
            "parent": "aba-dashboard",
            "drop": ["container"],
            "attr": {},
            "title": "Initial Title",
            "shape": {
                "xs": {
                    "width":1,
                    "height":1,
                    "min-width":1,
                    "min-height":1,
                    "max-height":9999,
                    "max-width":12
                }
            }
        }

### Definde Widget constraints based on the device the dashboard is rendered on
Devices sizes are abstracted into 5 sizes they are:

| Device size | Min width  | Mac width | description                            |
| ----------- | ---------- | --------- | -------------------------------------- |
| xl          | 1600px     | -         | Standart Desktop                       |
| l           | 1280px     | 1600px    | Small Desktop / Native UI integrated   |
| m           | 960px      | 1280px    | Tablet Landscape                       |
| s           | 460px      | 960px     | Tablet Portrait / Smartphone Landscape |
| xs          | -          | 460px     | Smartphone Portrait                    |

For every of these device sizes it is posible to add following constraints:

| Constraint | Description                                                         |
| ---------- | ------------------------------------------------------------------- |
| width      | The default width the widget will have when added to the dashboard  |
| height     | The default height the widget will have when added to the dashboard |
| min-width  | Widget can not be smaller than this                                 |
| min-height | Widget can not be smaller than this                                 |
| max-width  | Widget can not be bigger than this                                  |
| max-height | Widget can not be bigger than this                                  |

Each of this constraints is described in a abstract tile size. Also these constraints are inherited upwards, so if you only
define xs all bigger devices will use this constraint. These informations have to be placed in the shape object of the widget descriptor.

NOTE: It is not necessary at to define any constraint if none is defined default values will be used

    "width":1,
    "height":1,
    "min-width":1,
    "min-height":1,
    "max-height":9999,
    "max-width":12

### Modify the layout depending on current widget size
As a widget developer you should think about how to display information and controls on different screen sizes. Use event listeners to react on changes to the widget size.
```javascript
ready: function () {
    // Attach a callback function on resize
    window.addEventListener('resize', event => { this._layout() });
    // Debounce the same function for the inital display
    this.debounce('layout', this._layout, 10);
},
_layout: function () {
    // Get current size informations
    let clientRect = this.getBoundingClientRect();
}
```

## Running the tests (locally)
```bash
wct --configFile wct.conf.chrome.local.json
```
If WCT complains that the `xunit-reporter` plugin cannot be found, you may need to install it
```bash
npm install -g wct-xunit-reporter
```