/**
 * represent the matrix
 * has meta info about rows and cols
 */
class Matrix extends Array {
    set colCount (colCount) {
        this._colCount = colCount
    }
    get colCount () {
        return this._colCount
    }
    
    get rowCount () {
        return this.length
    }
}
class MatrixManager {

    
    constructor() {
        // use private property to implement a matrix cache
    }
    
    makeMatrix(widgetShapes, colCount) {

        // Validate input
        // validate col count
        if (!this._isValidCol(colCount)) {
            throw new Error(`Invalid matrix size definition. valid col (${colCount}): ${this._isValidCol(colCount)}.`)
        }
        // validate shapes
        widgetShapes.forEach(shape => {
            if (!this._isValidShape(shape)) {
                throw new Error(`Invalid matrix shape definition: ${JSON.stringify(shape)}`)
            }
        });

        let matrix = this._matrixOf(colCount, this.free)
        widgetShapes.forEach(shape => {
            // Append more rows if necessary
            let rowsNeeded = shape.row +  shape.height;
            while( rowsNeeded > matrix.rowCount ) {
                matrix.push(this._row(colCount));
            }
            this._placeShape(matrix, shape);
        });
        return matrix
    }

    _matrixOf(colCount) {
        let matrix = new Matrix();
        matrix.colCount = colCount;
        return matrix;
    }

    _row (colCount) {
        return new Array(colCount).fill(this.free);
    }   

    /**
     * Set an occupied mark to the matrix on all spots where the shape overspans 
     * @param {Array} matrix 
     * @param {object} shape - { "width":3, "height":2, "col":0, "row":2 } 
     */
    _placeShape(matrix, shape) {
        for (let rowIndex = 0; rowIndex < shape.height; rowIndex++) {
            for (let colIndex = 0; colIndex < shape.width; colIndex++) {
                
                // Column count is fixed and must not be oversized
                // If so its an error in the widget description
                if(matrix[shape.row + rowIndex].length <= shape.col + colIndex) {
                    //throw new Error('Widget is outside dashboard col!');
                    continue;
                }
                
                // Append rows dynamically
                if(matrix.length <= shape.row + rowIndex) {
                    //throw new Error('Widget is outside dashboard row!');
                    continue;
                }

                matrix[shape.row + rowIndex][shape.col + colIndex] = this.occupied
            }
        }
    }

    get free() {
        return 0
    }

    get occupied() {
        return 1
    }

    _isValidShape(shape) {
        return [shape.width, shape.height, shape.col, shape.row].every(Number.isInteger)
    }

    _isValidCol(col) {
        return Number.isInteger(col)
    }

}
